import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
   state: {
      lists: [
            {
               title: "Groot bereik",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/1.png",
               id: "1"
            },
            {
               title: "Gratis aanmelden",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/2.png",
               id: "2"
            },
            {
               title: "Klantenservice",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/3.png",
               id: "3"
            },
            {
               title: "Support en inzage",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/4.png",
               id: "4"
            },
            {
               title: "Verdien geld",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/5.png",
               id: "5"
            },
            {
               title: "Kwalitatief aanbod",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/6.png",
               id: "6"
            },
            {
               title: "Maatwerk oplossingen",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/7.png",
               id: "7"
            },
            {
               title: "Gemak",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/8.png",
               id: "8"
            }
         ]

   }
})